# Git hooks #

Git hooks são scripts que rodam automaticamente cada vez que você realiza alguma ação específica no repositório Git. Eles permitem modificar o comportamento original dos comandos Git e adicionar funcionalidades personalizadas em vários estados do ciclo de desenvolvimento.

Os Git Hooks são bastante aplicados para encorajar políticas de commit, alterar algo do ambiente dependendo do estado do repositório ou implementar fluxos para workflow de Integração Contínua.

Os scripts git hooks originais, que podem servir como modelo, estão disponívels em todo workdir de projeto, em .git/hooks. Para ativar uma configuração customizada por projeto, faz-se necessários algumas personalizações.
